#!/bin/bash

XAUTHORITY=$HOME/.Xauthority

HOST=$(hostname)
DISPLAY_NUMBER=$(echo $DISPLAY | cut -d. -f1 | cut -d: -f2)

AUTH_COOKIE=

AUTH_COOKIE=$(xauth -f $XAUTHORITY list | grep "^$(hostname)/unix:${DISPLAY_NUMBER} " | awk '{print $3}')

if [ "$AUTH_COOKIE" = "" ];then
	AUTH_COOKIE=$(xauth -f $XAUTHORITY list | grep "^$(hostname)/unix: " | awk '{print $3}')
fi

touch $HOME/.Xauthority

xauth -f $XAUTHORITY add ${HOST}/unix:${DISPLAY_NUMBER} MIT-MAGIC-COOKIE-1 $AUTH_COOKIE

/usr/bin/java -Dlog4j.configuration= -Djava.library.path=/usr/lib/x86_64-linux-gnu/jni:/opt/cplex_so/ -jar /opt/met4j-dynamicRFBA.jar "$@"
