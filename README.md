# Dynamic RFBA

## Installation

### Install singularity

https://sylabs.io/guides/3.3/user-guide/installation.html

### Create singularity image

```console
sudo singularity build dynamicRfba.sif dynamicRfba.def
```

This singularity image only contains GLPK solver, so the option "-sol CPLEX" won't work.

### Create singularity image with cplex

Dynamic-RFBA is compatible with Cplex 12.10. The license of CPLEX is proprietary, so we can't distribute it in this repository.

If you have downloaded cplex 12.10, first you need to create a directory cplex_so where you copy the .so files needed for cplex java.

Example :

```console
mkdir cplex_so
cp -R /usr/local/ILOG/CPLEX_Studio1210/cplex/bin/x86-64_linux/*so cplex_so
tar zcvf cplex_so.tgz cplex_so
```

After, you can build the image that will integrate the .so files:

```console
sudo singularity build dynamicRfbaWithCplex.sif dynamicRfbaWithCplex.def
```

## Launch program

Prints the usage:

```console
./dynamicRfba.sif -h 
```
